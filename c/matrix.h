#ifndef __MATRIX_H__// this is an include guard
#define __MATRIX_H__

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#define MATRIX_MAX_WIDTH  20
#define MATRIX_MAX_HEIGHT 20

typedef struct matrix_s
{
	int32_t elements[MATRIX_MAX_WIDTH * MATRIX_MAX_HEIGHT];
	size_t  width;
	size_t  height;
} matrix_t;

static const matrix_t matrix_empty =
{
	.elements = {0},
	.width    = 0,
	.height   = 0
};

/* Matrix constructor
 *
 * matrix_string - NUL terminated character string of the form "9 8 7\n5 3 2\n6 6 7\0"
 * returns matrix (matrix_empty if there was some failure)
 */
matrix_t matrix_create(const char* matrix_string);

/* Prints matrix to stdout
 *
 * matrix - to print
 */
void matrix_print(const matrix_t* matrix);

/* Checks if two matrices are equal
 * a & b - matrices to check equivalence for
 * returns equivalence bool
 */
bool matrix_equal(const matrix_t* a, const matrix_t* b);

/* Gets matrix row `index`
 *
 * matrix - pointer to matrix struct
 * index  - row index
 * values - array provided by user to be populated by row data
 * len    - max length of row array
 * returns length of row (0 if failure)
 */
size_t matrix_row(const matrix_t* matrix, unsigned index,
                  int32_t* values, size_t len);

/* Gets matrix column `index`
 *
 * matrix - pointer to matrix struct
 * index  - column index
 * values - array provided by user to be populated by column data
 * len    - max length of column array
 * returns length of column (0 if failure)
 */
size_t matrix_col(const matrix_t* matrix, unsigned index,
                  int32_t* values, size_t len);

#endif