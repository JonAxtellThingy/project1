#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

#include "matrix.h"

static void check_arrays_equal(int32_t* array_1, int32_t* array_2, size_t len)
{
	for (unsigned i = 0; i < len; i++)
		assert(array_1[i] == array_2[i]);
}

static void test_matrix_print(void)
{
	printf("---test_matrix_print\n");
	matrix_t my_matrix = matrix_create("9 8 7\n5 3 2\n6 6 7");

	matrix_print(&my_matrix);
}

static void test_matrix_get_rows(void)
{
	printf("---test_matrix_get_rows\n");
	matrix_t my_matrix = matrix_create(
		"9 -28 -988 100\n1 1 3876 2\n6 6 7 7");

	assert((my_matrix.height == 3) && (my_matrix.width == 4));

	int32_t row_0_expected[4] = {9, -28, -988, 100};
	int32_t row_0[4];

	size_t width = matrix_row(&my_matrix, 0, row_0, 4);

	assert(width == my_matrix.width);

	check_arrays_equal(row_0_expected, row_0, 4);

	printf("row 1 of\n");
	matrix_print(&my_matrix);
	printf("\nis: [");
	for (unsigned i = 0; i < 4; i++)
		printf("%d%s", row_0[i], i < 3 ? ", ": "]\n");

}

static void test_matrix_get_cols(void)
{
	printf("---test_matrix_get_cols\n");
	matrix_t my_matrix = matrix_create(
		"1 2 3 4 5\n-1 -2 -3 -4 -5\n100 0 0 0 0\n0 0 0 0 0");

	assert((my_matrix.height == 4) && (my_matrix.width == 5));

	int32_t col_1_expected[4] = {2, -2, 0, 0};
	int32_t col_1[4];

	size_t height = matrix_col(&my_matrix, 1, col_1, 4);

	assert(height == my_matrix.height);

	check_arrays_equal(col_1_expected, col_1, 4);

	printf("col 1 of\n");
	matrix_print(&my_matrix);
	printf("\nis: [");
	for (unsigned i = 0; i < 4; i++)
		printf("%d%s", col_1[i], i < 3 ? ", ": "]\n");
}

static void test_matrix_malformed(void)
{
	printf("---test_matrix_malformed\n");
	matrix_t bad_matrix = matrix_create(
		"this is a malformed\nmatrix\n100 0 0 0 0\n0 0 0 0 0");

	assert(matrix_equal(&bad_matrix, (matrix_t*)&matrix_empty));

	bad_matrix = matrix_create("1 2 3 4\n1 2 3");

	assert(matrix_equal(&bad_matrix, (matrix_t*)&matrix_empty));


}

int main(int argc, char* argv[])
{
	(void)argc;
	(void)argv;

	printf("Running tests...\n\n");

	test_matrix_print();
	test_matrix_get_rows();
	test_matrix_get_cols();
	test_matrix_malformed();

	printf("\nAll tests passed!!!\n");
}
